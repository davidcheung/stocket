<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Stocket</title>

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/stocket.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<meta name="viewport" content="width=device-width, user-scalable=no">
<style>
::-webkit-input-placeholder {
   color: #B1C12A;
}

:-moz-placeholder { /* Firefox 18- */
   color: #B1C12A;  
}

::-moz-placeholder {  /* Firefox 19+ */
   color: #B1C12A;  
}

:-ms-input-placeholder {  
   color: #B1C12A;  
}
</style>
<head>
	
<body class="login-body">
	

	<div style="text-align:center">
		<div class="login-page-center-box">

			<div class="login-welcome">
				Welcome to
			</div>
			<div class="login-main-logo">
				<img src="images/assets/logo.png" alt="Stocket">
			</div>
			<div class="login-welcome">
				The App that takes grocery<br/> stock from your pictures
			</div>
		
			{{ Form::open(array('url' => 'login')) }}

				<!-- if there are login errors, show them here -->
				<p>
					{{ $errors->first('email') }}
					{{ $errors->first('password') }}
				</p>

				<div class="login-panel login-form-group">
					<div class="username" >
						<input type="text" placeholder="Username" >
					</div>
					<hr/>
					<div class="password">
						<input type="text" placeholder="Password">
					</div>
				</div>

				<div class="login-form-group login-remember">
					<label>
						<input type="checkbox"> 
						<span>Remember me</span>
					</label>
				</div>

				<input class="login-button login-form-group" type="submit" value="LET'S TAKE STOCK">
			{{ Form::close() }}

		</div>
	</div>

</body>
</html>
