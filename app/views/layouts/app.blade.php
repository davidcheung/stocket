<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Stocket</title>
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/stocket.css">
	<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body class="app">
	<div class="navbar navbar-inverse navbar-stocket navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header pull-left">
            <a class="navbar-brand" href="#">Stocket</a>
        </div>
        <div class="navbar-header pull-right">

          	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
	     	</button>
          
        </div>
        <div class="navbar-header pull-right nav-constant">
            <ul class="nav navbar-nav">
                <li><a href="#"><i class="glyphicon glyphicon-home"></i></a></li>
            </ul>
        </div>
        <div class="navbar-header pull-right nav-constant">
            <ul class="nav navbar-nav">
                <li><a href="#"><i class="glyphicon glyphicon-camera"></i></a></li>
            </ul>
        </div>


        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Dashboard</a></li>
            <li><a href="#about">Profile</a></li>
            <li><a href="#contact">Logout</a></li>
          </ul>
        </div><!--/.nav-collapse -->
       
      </div>
    </div>
	
</body>
</html>