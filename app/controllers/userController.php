<?php

class userController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showLogin(){
		// show the form
		return View::make('users/login');
	}

	public function doLogin(){
		// process the form
		return View::make('dashboard');
	}

	public function showRegister(){
		return View::make('user.register', data, mergeData);
	}

}
